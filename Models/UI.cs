using System.Text.RegularExpressions;
using System;
using System.Linq;
using System.Collections.Generic;
namespace Core;

public class UI
{

    static string num_pattern = @"^(?<number>[-+]?[0-9]+(\.[0-9]+)?([eE][-+]?[0-9]+)?)$";
    static string[] key_words = { "IF", "THEN", "ELSIF", "END", "OR", "NOT", "AND", };


    public static GUI_Report GetGuiReport(Report r)
    {
        string error_report = "";
        string ids_report = "";
        string numbers_report = "";
        string semantic_warnings = "";
				Console.WriteLine($"Отладка {r.Pointer}");


        if (r.Error.HasValue)
        {
            List<string> lines = new List<string>();
            var err = r.Error.Value;
            						
						string error_line = "";

						error_line += r.InputLine.Substring(0,r.Pointer);
						error_line += $"<{r.InputLine[r.Pointer]}>";
						error_line += r.InputLine.Substring(r.Pointer+1, (r.InputLine.Length-1)-r.Pointer);
						error_line += $"\nPointer:{r.Pointer}";
						
						
            lines.Add(error_line);
            lines.Add($"Состояние: {r.StatesLog.Last()}");

            error_report = string.Join('\n', lines.ToArray());

        }
        else
        {
						error_report = "Строка соответсвует грамматике.\n";
            var sf = GetSemanticReport(r.InputLine);
            SemanticType[] toprint = { SemanticType.Number, SemanticType.ID };

            foreach (SemanticType type in toprint)
            {
                var names = sf.Where(s => s.Type == type).Select(s => s.Text);
                if (names.Any())
                {
                    if (type == SemanticType.ID)
                    {
                        foreach (string name in names)
                        {
                            string report = name;
                            if (name.Length > 8) semantic_warnings += $"{name} - Длина ID должно быть небольше 8 знаков!\n";
                            ids_report += $"{report}\n";
                        }

                    }
                    else if (type == SemanticType.Number)
                    {
                        foreach (string name in names)
                        {
                            numbers_report += $"{name.Trim()} - {GetNumberReport(name)}\n";
                        }
                    }
                }
            }

        }

        return new GUI_Report()
        {
            ErrorReport = error_report + semantic_warnings,
            NumbersReport = numbers_report,
            IDReport = ids_report,
						IsSuccess = r.Error.HasValue
        };
    }


    private static string GetNumberReport(string num)
    {
        string report = "";


        bool is_integer, is_float, is_decimal;

        is_decimal = Regex.Match(num.Trim(), @"^[-+]?[0-9]+\.[0-9]+[eE][-+]?[0-9]+$").Success;
        is_integer = Regex.Match(num.Trim(), @"^[-+]?[0-9]+$").Success;
        is_float = Regex.Match(num, @"^[-+]?[0-9]+\.[0-9]+$").Success;

        if (is_integer)
        {

            int number;
            bool succes = int.TryParse(num, out number);

            if (succes)
            {
                bool in_range = (number < short.MaxValue) & (number > short.MinValue) ? true : false;
								report += $"Целое число";
                if (!in_range) report += " Вне диапазона допустимых значений!";
            }
            else
            {
                report += $"Слишком большое число";
            }
        }
        else if (is_decimal)
        {
            report = $"Число с фиксированной точкой";
            var pices = num.Split('E', 'e');
            if (pices[0].Length > 15) report += "	Слишком много чисел в мантисе";
        }
        else if (is_float) report = $"Число с плавающей точкой";
        else report = $"Неизвестный тип";


        return report;
    }


    private static List<SemanticFragment> GetSemanticReport(string line)
    {
        string[] fragments = line.Split(' ', '(', ')', '=', '>', '<', ':', ';', '[', ']');

        List<SemanticFragment> semanticFragments = new List<SemanticFragment>();

        foreach (string fragment in fragments)
        {
            if (!string.IsNullOrWhiteSpace(fragment))
            {
                SemanticType type = SemanticType.Unknown;
                Match num_match = Regex.Match(fragment.Trim(), num_pattern);

                if (num_match.Success) type = SemanticType.Number;
                else if (key_words.Contains(fragment)) type = SemanticType.KeyWord;
                else type = SemanticType.ID;

                if (!semanticFragments.Where(f => f.Text == fragment.Trim()).Any())
                {
                    semanticFragments.Add(new SemanticFragment() { Text = fragment.Trim(), Type = type });
                }
            }
        }

        return semanticFragments;
    }

    public struct SemanticFragment
    {
        public string Text { get; init; }
        public SemanticType Type { get; init; }
    }


    public enum SemanticType
    {
        Number, ID, KeyWord, Unknown
    }


    public struct GUI_Report
    {
        public string ErrorReport { get; init; }
        public string NumbersReport { get; init; }
        public string IDReport { get; init; }
        public bool IsSuccess { get; init; }
    }

}
